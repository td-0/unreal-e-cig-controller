# Unreal E-Cig Controller
## Project to create an E-cig that doesn’t bring any smoke into a users lungs and instead encourages smoking cessation through a satisfying physical/visual and auditory experience

This project uses a light sensor attached to the LED of an E-Cig to determine when user is pulling and trigger a state machine. The included Arduino script reads in the light sensor data and outputs to serial monitor (as the Arduino Uno used can only output to serial monitor). 

The output is read by the included Python script, which interprets it to a keyboard press. This keyboard press can then be read into a game engine as input. There are two included Unreal Engine 5 demos for this, one in VR and one standard.
